using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using MaoIndyAPI.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace MaoIndyAPI.Controllers {
    [Route("api/[controller]")]
    public class AuthController : Controller {
        [HttpPost, Route("login")]
        public IActionResult Login([FromBody] LoginModel login) {
            if (login == null) {
                return BadRequest("invalid client request");
            }

            if (login.UserName == "johndoe" && login.Password == "def@123") {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var tokeOptions = new JwtSecurityToken(
                    issuer: "http://localhost:5000",
                    audience: "http://localhost:5000",
                    claims : new List<Claim>(),
                    expires : DateTime.Now.AddMinutes(5),
                    signingCredentials : signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return Ok(new { Token = tokenString });
            } else {
                return Unauthorized();
            }
        }
    }
}